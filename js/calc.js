'use_strict';
//Created by Namors14
//v 1.01
//snamor14@gmail.com

var production = 'corner';
var mark = {
	chermet: [
		//{ name: 'Ст 3', value: 7800 }, // original value
		{ name: 'Ст 3', value: 7850 },
		{ name: '10', value: 7850 },
		{ name: '20', value: 7850 },
		{ name: '40Х', value: 7820 },
		{ name: '45', value: 7820 },
		{ name: '65', value: 7810 },
		{ name: '65Г', value: 7850 },
		{ name: '09Г2С', value: 7850 },
		{ name: '15Х5М', value: 7750 },
		{ name: '10ХСНД', value: 7850 },
		{ name: '12Х1МФ', value: 7800 },
		{ name: 'ШХ15', value: 7810 },
		{ name: 'Р6М5', value: 8300 },
		{ name: 'У7', value: 7830 },
		{ name: 'У8', value: 7840 },
		{ name: 'У8А', value: 7830 },
		{ name: 'У10', value: 7810 },
		{ name: 'У10А', value: 7810 },
		{ name: 'У12А', value: 7810 }
	],
	aluminium: [
		{ name: 'А5', value: 2710 },
		{ name: 'АД', value: 2710 },
		{ name: 'АД1', value: 2710 },
		{ name: 'АК4', value: 2770 },
		{ name: 'АК6', value: 2750 },
		{ name: 'АМг', value: 2670 },
		{ name: 'АМц', value: 2730 },
		{ name: 'В95', value: 2850 }
	],
	cuprum: [
		{ name: 'М1', value: 8910 },
		{ name: 'М2', value: 8930 },
		{ name: 'М3', value: 8930 }
	],
	brass: [
		{ name: 'Л63', value: 8440 },
		{ name: 'Л68', value: 8600 },
		{ name: 'ЛЖМц59-1-1', value: 8500 },
		{ name: 'ЛМц58-2', value: 8400 },
		{ name: 'ЛС58-2', value: 8450 },
		{ name: 'ЛС59-1', value: 8450 },
		{ name: 'ЛС63-3', value: 8500 }
	],
	bronze: [
		{ name: 'БрАЖ9-4', value: 7600 },
		{ name: 'БрАЖМц10-3-1,5', value: 7800 },
		{ name: 'БрАЖН10-4-4', value: 7500 },
		{ name: 'БрАМц9-2', value: 7600 },
		{ name: 'БрБ2', value: 8200 },
		{ name: 'БрКМЦ3-1', value: 8400 },
		{ name: 'БрОФ7-0,2', value: 7800 },
		{ name: 'БрОЦС5-5-5', value: 8800 }
	],
	titanium: [
		{ name: 'ВТ1-00', value: 4505 },
		{ name: 'ВТ1-1', value: 4505 },
		{ name: 'АТ-6', value: 4430 },
		{ name: 'ВТ14', value: 4520 },
		{ name: 'ВТ20', value: 4450 },
		{ name: 'ВТ22', value: 4600 },
		{ name: 'ВТ3-1', value: 4500 },
		{ name: 'ВТ5', value: 4400 },
		{ name: 'ВТ5-1', value: 4460 },
		{ name: 'ВТ6', value: 4430 },
		{ name: 'ОТ4-1', value: 4550 },
		{ name: 'ПТ7М', value: 4490 }
	]
};

var channelTypes = [
	{ number: '5П', weight: 4.84 },
	{ number: '5У', weight: 4.84 },
	{ number: '6,5П', weight: 5.9 },
	{ number: '6,5У', weight: 5.9 },
	{ number: '8П', weight: 7.05 },
	{ number: '8У', weight: 7.05 },
	{ number: '10П', weight: 8.59 },
	{ number: '10У', weight: 8.59 },
	{ number: '12П', weight: 10.4 },
	{ number: '12У', weight: 10.4 },
	{ number: '14П', weight: 12.3 },
	{ number: '14у', weight: 12.3 },
	{ number: '16аП', weight: 15.3 },
	{ number: '16П', weight: 14.2 },
	{ number: '16у', weight: 14.2 },
	{ number: '18аП', weight: 17.4 },
	{ number: '18аУ', weight: 17.4 },
	{ number: '18П', weight: 16.3 },
	{ number: '18у', weight: 16.3 },
	{ number: '20П', weight: 18.4 },
	{ number: '20У', weight: 18.4 },
	{ number: '20У', weight: 21 },
	{ number: '22П', weight: 21 },
	{ number: '24П', weight: 24 },
	{ number: '24У', weight: 24 },
	{ number: '27П', weight: 27.7 },
	{ number: '30П', weight: 31.8 },
	{ number: '30У', weight: 31.8 },
	{ number: '33П', weight: 36.5 },
	{ number: '33У', weight: 36.5 },
	{ number: '36П', weight: 41.9 },
	{ number: '36у', weight: 41.9 },
	{ number: '40П', weight: 48.3 },
	{ number: '40У', weight: 48.3 }
];

var balkTypes = [
	{
		name: 'Гост 8239-89',
		numbers: [
			{ name: '10', weight: 9.46 },
			{ name: '12', weight: 11.5 },
			{ name: '14', weight: 13.7 },
			{ name: '16', weight: 15.9 },
			{ name: '18', weight: 18.4 },
			{ name: '20', weight: 21.0 },
			{ name: '22', weight: 24.0 },
			{ name: '24', weight: 27.3 },
			{ name: '27', weight: 31.5 },
			{ name: '30', weight: 36.5 },
			{ name: '33', weight: 42.2 },
			{ name: '36', weight: 48.6 },
			{ name: '40', weight: 57.0 },
			{ name: '45', weight: 67.0 },
			{ name: '50', weight: 79.0 },
			{ name: '55', weight: 93.0 },
			{ name: '60', weight: 108.0 }
		]
	},
	{
		name: 'Нормальный двутавр (Б)',
		numbers: [
			{ name: '10Б1', weight: 8.1 },
			{ name: '12Б1',	weight: 8.7	},
			{ name: '12Б2',	weight: 10.4 },
			{ name: '14Б1',	weight: 10.5 },
			{ name: '14Б2',	weight: 12.9 },
			{ name: '16Б1',	weight: 12.7 },
			{ name: '16Б2',	weight: 15.8 },
			{ name: '18Б1',	weight: 15.4 },
			{ name: '18Б2',	weight: 18.8 },
			{ name: '20Б1',	weight: 22.4 },
			{ name: '23Б1',	weight: 25.8 },
			{ name: '26Б1',	weight: 28.0 },
			{ name: '26Б2',	weight: 31.2 },
			{ name: '30Б1',	weight: 32.9 },
			{ name: '30Б2',	weight: 36.6 },
			{ name: '35Б1',	weight: 38.9 },
			{ name: '35Б2',	weight: 43.3 },
			{ name: '40Б1',	weight: 48.1 },
			{ name: '40Б2',	weight: 55.0 },
			{ name: '45Б1',	weight: 60.0 },
			{ name: '45Б2',	weight: 68.0 },
			{ name: '50Б1',	weight: 73.0 },
			{ name: '50Б2',	weight: 81.0 },
			{ name: '55Б1',	weight: 89.0 },
			{ name: '55Б2',	weight: 98.0 },
			{ name: '60Б1',	weight: 106.0 },
			{ name: '60Б2',	weight: 116.0 },
			{ name: '70Б1',	weight: 129.0 },
			{ name: '70Б2',	weight: 144.0 },
			{ name: '80Б1',	weight: 160.0 },
			{ name: '80Б2',	weight: 178.0 },
			{ name: '90Б1',	weight: 194.0 },
			{ name: '90Б2',	weight: 214.0 },
			{ name: '100Б1', weight: 231.0 },
			{ name: '100Б2', weight: 258.0 },
			{ name: '100Б3', weight: 286.0 },
			{ name: '100Б4', weight: 315.0 }
		]
	},
	{
		name: 'Широкополочный двутавр (Ш)',
		numbers: [
			{ name: '20Ш1',	weight: 30.6 },
			{ name: '23Ш1',	weight: 36.2 },
			{ name: '26Ш1',	weight: 42.7 },
			{ name: '26Ш2',	weight: 49.2 },
			{ name: '30Ш1',	weight: 54.0 },
			{ name: '30Ш2',	weight: 61.0 },
			{ name: '30Ш3',	weight: 68.0 },
			{ name: '35O1',	weight: 75.0 },
			{ name: '35Ш2',	weight: 82.0 },
			{ name: '35Ш3',	weight: 91.0 },
			{ name: '40Ш1',	weight: 96.0 },
			{ name: '40Ш2',	weight: 111.0 },
			{ name: '40Ш3',	weight: 123.0 },
			{ name: '50Ш1',	weight: 114.0 },
			{ name: '50Ш2',	weight: 139.0 },
			{ name: '50Ш3',	weight: 156.0 },
			{ name: '50Ш4',	weight: 174.0 },
			{ name: '60Ш1',	weight: 142.0 },
			{ name: '60Ш2',	weight: 177.0 },
			{ name: '60ШЗ',	weight: 206.0 },
			{ name: '60Д14',weight: 234.0 },
			{ name: '70Ш1',	weight: 170.0 },
			{ name: '70Ш2',	weight: 198.0 },
			{ name: '70ШЗ',	weight: 235.0 },
			{ name: '70Ш4',	weight: 261.0 },
			{ name: '70Ш5',	weight: 306.0 }
		]
	},
	{
		name: 'Колонный двутавр (К)',
		numbers: [
			{ name: '20K1',	weight: 41.5 },
			{ name: '20K2',	weight: 46.9 },
			{ name: '23K1',	weight: 52.0 },
			{ name: '23K2',	weight: 60.0 },
			{ name: '26K1',	weight: 65.0 },
			{ name: '26K2',	weight: 73.0 },
			{ name: '26K3',	weight: 83.0 },
			{ name: '30K1',	weight: 85.0 },
			{ name: '30K2',	weight: 96.0 },
			{ name: '30К3',	weight: 109.0 },
			{ name: '35К1',	weight: 110.0 },
			{ name: '35К2',	weight: 126.0 },
			{ name: '35K3',	weight: 145.0 },
			{ name: '40К1',	weight: 138.0 },
			{ name: '40К2',	weight: 166.0 },
			{ name: '40K3',	weight: 202.0 },
			{ name: '40К4',	weight: 242.0 },
			{ name: '40К5',	weight: 291.0 }
		]
	},
	{
		name: 'Двутавр дополнительной серии (Д)',
		numbers: [
			{ name: '24ДБ1',	weight: 27.8 },
			{ name: '27ДБ1',	weight: 31.9 },
			{ name: '36ДБ1',	weight: 49.1 },
			{ name: '35ДБ1',	weight: 33.6 },
			{ name: '40ДБ1',	weight: 39.7 },
			{ name: '45ДБ1',	weight: 53.0 },
			{ name: '45ДБ2',	weight: 65.0 },
			{ name: '30ДШ1',	weight: 73.0 },
			{ name: '40ДШ1',	weight: 124.0 },
			{ name: '50ДШ1',	weight: 155.0 }
		]
	},
	{
		name: 'Сварной двутавр (С)',
		numbers: [
			{ name: '45БС1',	weight: 64.0 },
			{ name: '45БС2',	weight: 134.0 },
			{ name: '45БС3',	weight: 66.0 },
			{ name: '50БС1',	weight: 86.0 },
			{ name: '50БС2',	weight: 118.0 },
			{ name: '50БС3',	weight: 160.0 },
			{ name: '50БС4',	weight: 191.0 },
			{ name: '55БС1',	weight: 103.0 },
			{ name: '55БС2',	weight: 91.0 },
			{ name: '60БС1',	weight: 112.0 },
			{ name: '60БС2',	weight: 127.0 },
			{ name: '60БС3',	weight: 152.0 },
			{ name: '60БС4',	weight: 186.0 },
			{ name: '60БС5',	weight: 219.0 },
			{ name: '60БС6',	weight: 101.0 },
			{ name: '70БС1',	weight: 142.0 },
			{ name: '70БС2',	weight: 171.0 },
			{ name: '70БС3',	weight: 197.0 },
			{ name: '70БС4',	weight: 232.0 },
			{ name: '70БС5',	weight: 302.0 },
			{ name: '70БС6',	weight: 120.0 },
			{ name: '80БС1',	weight: 162.0 },
			{ name: '80БС2',	weight: 248.0 },
			{ name: '90БС1',	weight: 202.0 },
			{ name: '90БС2',	weight: 277.0 },
			{ name: '100БС1',	weight: 244.0 },
			{ name: '100БС2',	weight: 269.0 },
			{ name: '100БС3',	weight: 329.0 },
			{ name: '120БС1',	weight: 242.0 },
			{ name: '120БС2',	weight: 278.0 },
			{ name: '140БС1',	weight: 258.0 },
			{ name: '140БС2',	weight: 273.0 },
			{ name: '140БС3',	weight: 350.0 },
			{ name: '160БС1',	weight: 292.0 },
			{ name: '160БС2',	weight: 308.0 },
			{ name: '160БС3',	weight: 372.0 },
			{ name: '160БС4',	weight: 396.0 },
			{ name: '180БС1',	weight: 385.0 },
			{ name: '180БС2',	weight: 389.0 },
			{ name: '180БС3',	weight: 428.0 },
			{ name: '180БС4',	weight: 502.0 },
			{ name: '200БС1',	weight: 404.0 },
			{ name: '200БС2',	weight: 480.0 },
			{ name: '200БС3',	weight: 528.0 }
		]
	}
];

$(function(){

		$('#material').change(function() {
			var select = $('#mark');
			if(select.prop) {
				var options = select.prop('options');
				}
				else {
				var options = select.attr('options');
				}
			select.empty();
			var material = $(this).val();
			var options = [];
			if (material in mark) {
				$('#mark').prop('disabled', false);
				$.each(mark[material], function(index, elem) {
					options[options.length] = new Option(elem.name, elem.value);
				});
			} else {
				$('#mark').prop('disabled', 'disabled');
			}
			$('#mark').html(options);
		});

		$('#material').change();
        
        $('.product').on('click', function() {
		 let prod = $(this).attr("id");
		 out(production);
    	production = prod;
         select(prod);
         $( "#field" ).empty();
         switch(prod) {
    		case "corner":
			$("#field").append('<div class="row"><div><span><label>Высота (мм)</label><input id="input-height" type="text" placeholder="0" name="height"></span></div><div><span><label for="input-height">Толщ. стенки (мм)</label><input type="text" placeholder="0" name="tolst"></span></div><div class="output"><div class="lenght"><span><label for="input-height">Длина (м)</label><input type="text" placeholder="0" name="lenght"></span></div><div class="weight"><span><label for="input-height">Вес (кг)</label><input type="text" placeholder="0" name="weight"></span></div></div></div>');   		
			break;
    		case "sheet":
    		$("#field").append('<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-12"><span><label>Толщина листа (мм)</label><input type="text" placeholder="0" name="tolst"></input></span></div><div class="col-md-12"><span><label>Ширина листа (мм)</label><input type="text" placeholder="0" name="width"></input></span></div><div class="col-md-12 lenght"><span><label>Длина листа (мм)</label><input type="text" placeholder="0" name="lenght"></input></span></div></div></div><div class="col-md-12 output"><div class="row"><div class="col-md-12 square"></div><div class="col-md-12 weight"><span><label>Вес (кг)</label><input type="text" placeholder="0" name="weight"></input></span></div></div></div></div>');
    		break;
    		case 'pipe':
    		$("#field").append('<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-12"><span><label>Внешн. диаметр (мм)</label><input type="text" placeholder="0" name="diametr"></input></span></div><div class="col-md-12"><span><label>Толщ. стенки (мм)</label><input type="text" placeholder="0" name="tolst"></input></span></div></div></div><div class="col-md-12 output"><div class="row"><div class="col-md-12 lenght"><span><label>Длина (м)</label><input type="text" placeholder="0" name="lenght"></input></span></div><div class="col-md-12 weight"><span><label>Вес (кг)</label><input type="text" placeholder="0" name="weight"></input></span></div></div></div></div>');
    		break;
    		case 'circle':
    		$("#field").append('<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-12"><span><label>Диаметр (мм)</label><input type="text" placeholder="0" name="diameter"></input></span></div></div></div><div class="col-md-12 output"><div class="row"><div class="col-md-12 lenght"><span><label>Длина (м)</label><input type="text" placeholder="0" name="lenght"></input></span></div><div class="col-md-12 weight"><span><label>Вес (кг)</label><input type="text" placeholder="0" name="weight"></input></span></div></div></div></div>');  
    		break;
    		case 'pipe_prof':
    		$("#field").append('<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-12"><span><label>Высота (мм)</label><input type="text" placeholder="0" name="height"></input></span></div><div class="col-md-12"><span><label>Ширина (мм)</label><input type="text" placeholder="0" name="width"></input></span></div><div class="col-md-12"><span><label>Толщина стенки (мм)</label><input type="text" placeholder="0" name="tolst"></input></span></div></div></div><div class="col-md-12 output"><div class="row"><div class="col-md-12 lenght"><span><label>Длина (м)</label><input type="text" placeholder="0" name="lenght"></input></span></div><div class="col-md-12 weight"><span><label>Вес (кг)</label><input type="text" placeholder="0" name="weight"></input></span></div></div></div></div>');
     		break;
    		case 'square':
    		$("#field").append('<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-12"><span><label>Ширина (мм)</label><input type="text" placeholder="0" name="width"></input></span></div></div></div><div class="col-md-12 output"><div class="row"><div class="col-md-12 lenght"><span><label>Длина (м)</label><input type="text" placeholder="0" name="lenght"></input></span></div><div class="col-md-12 weight"><span><label>Вес (кг)</label><input type="text" placeholder="0" name="weight"></input></span></div></div></div></div>');
			break;
    		case 'channel':
			$("#field").append('<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-12"><span><label>Номер швеллера</label><select class="param-select channelType" name="channelType"></select></div></div><div class="col-md-12 output"><div class="row"><div class="col-md-12 lenght"><span><label>Длина (м)</label><input type="text" placeholder="0" name="lenght"></input></span></div><div class="col-md-12 weight"><span><label>Вес (кг)</label><input type="text" placeholder="0" name="weight"></input></span></div></div></div></div>');

			var options = [];
			$('#mark').prop('disabled', false);
			$.each(channelTypes, function(index, elem) {
				options[options.length] = new Option(elem.number, elem.weight);
			});

			$('.channelType').html(options);

    		break;
    		case 'strip':
    		$("#field").append('<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-12"><span><label>Толщина (мм)</label><input type="text" placeholder="0" name="tolst"></input></span></div><div class="col-md-12"><span><label>Ширина (мм)</label><input type="text" placeholder="0" name="width"></input></span></div></div></div><div class="col-md-12 output"><div class="row"><div class="col-md-12 lenght"><span><label>Длина (м)</label><input type="text" placeholder="0" name="lenght"></input></span></div><div class="col-md-12 square"></div><div class="col-md-12 weight"><span><label>Вес (кг)</label><input type="text" placeholder="0" name="weight"></input></span></div></div></div></div>');
			break;
    		case 'balk':
			 $("#field").append('<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-12"><span><label>Тип балки</label><select class="param-select balkTypes"><option>Тип</option></select></span></div><div class="col-md-12"><span><label>Номер балки</label><select class="param-select balkNumbers" name="balkNumber"><option>Номер</option></select></span></div><div class="col-md-12 output"><div class="row"><div class="col-md-12 lenght"><span><label>Длина (м)</label><input type="text" placeholder="0" name="lenght"></input></span></div><div class="col-md-12 weight"><span><label>Вес (кг)</label><input type="text" placeholder="0" name="weight"></input></span></div></div></div></div>');

			 var options = [];

			 $('#mark').prop('disabled', false);
			 $.each(balkTypes, function(index, elem) {
		 		options[options.length] = new Option(elem.name, index);
			  });

			  $('.balkTypes').html(options);
			  
			 $('.balkTypes').change(function() {
				var balkType = $(this).val();
				options = [];
				$.each(balkTypes[balkType].numbers, function(index, elem) {
					options[options.length] = new Option(elem.name, elem.weight);
				});
				$('.balkNumbers').empty();
				$('.balkNumbers').html(options);
			});

			$('.balkTypes').change();

    		break;
    		case 'hexahedron':
    		$("#field").append('<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-12"><span><label>Номер(диаметр) (мм)</label><input type="text" placeholder="0" name="diametr"></input></span></div></div></div><div class="col-md-12 output"><div class="row"><div class="col-md-12 lenght"><span><label>Длина (м)</label><input type="text" placeholder="0" name="lenght"></input></span></div><div class="col-md-12 weight"><span><label>Вес (кг)</label><input type="text" placeholder="0" name="weight"></input></span></div></div></div></div>');
    		break;
    	}
     });
});


function handler(event) {

	function str(el) {

		for(let i=1;i<11;i++) {
			let prod = search1(i);
			if(el.indexOf(prod) + 1) 
			{
				return prod;
			}
		}
  }

  function search1(iner) {
  	switch(iner)
	{
		case 1:
		return 'corner';
		break;
		case 2:
		return 'sheet';
		break;
		case 3:
		return 'pipe';
		break;
		case 4:
		return 'circle';
		break;
		case 5:
		return 'pipe_prof';
		break;
		case 6:
		return 'square';
		break;
		case 7:
		return 'channel';
		break;
		case 8:
		return 'strip';
		break;
		case 9:
		return 'balk';
		break;
		case 10:
		return 'hexahedron';
		break;
	}
  }

	if (event.type == 'mouseover') {
		let new1 = event.target.className;
		select(str(new1));
	 }
	 if(event.type == 'mouseout') {
	 	let new1 = event.target.className;
		out(str(new1));
	 }
}
function select(prod) {
	let noumber = type(prod);
	        $('#'+prod).addClass("selected");
}
function out(prod) {
	let noumber = type(prod);
			$('#'+prod).removeClass("selected");
}
function type(product)
{
	switch(product)
	{
		case 'corner':
		return 1;
		break;
		case 'sheet':
		return 2;
		break;
		case 'pipe':
		return 3;
		break;
		case 'circle':
		return 4;
		break;
		case 'pipe_prof':
		return 5;
		break;
		case 'square':
		return 6;
		break;
		case 'channel':
		return 7;
		break;
		case 'strip':
		return 8;
		break;
		case 'balk':
		return 9;
		break;
		case 'hexahedron':
		return 10;
		break;
	}
}

var plotnost = {
	steel: 7850,
	chugun: 7200,
	aluminium: 2700,
	bronze: 8300,
	brass: 8450,
	magnessium: 1740,
	cuprum: 8900,
	nickel: 8800,
	tin: 7280,
	plumbum: 11350,
	titanium: 4500,
	zinc: 7135
}

function density(mat)
{
	return plotnost[mat];
}

function calculation(obj)
{
	let plotn;

	if (obj.mark.value) {
		plotn = obj.mark.value;
	} else {
		plotn = density(obj.material.value);
	}

	switch(production)
	{
		case 'corner':
		corner(obj,plotn);
		break;
		case 'sheet':
		sheet(obj,plotn);
		break;
		case 'pipe':
		pipe(obj,plotn);
		break;
		case 'circle':
		circle(obj,plotn);
		break;
		case 'pipe_prof':
		pipe_prof(obj,plotn);
		break;
		case 'square':
		square(obj,plotn);
		break;
		case 'channel':
		channel(obj,plotn);
		break;
		case 'strip':
		strip(obj,plotn);
		break;
		case 'balk':
		balk(obj,plotn);
		break;
		case 'hexahedron':
		hexahedron(obj,plotn);
		break;
	}


}

function radius_zao(a)
{
	var arr = [];
	if(a<=25) { arr[0]=3.5; arr[1]=1.2; }
	  else if(a<=30) { arr[0]=4; arr[1]=1.3; }
	    else if(a<=45) { arr[0]=5; arr[1]=1.7; }
	      else if(a<=56) { arr[0]=6; arr[1]=2; }
	        else if(a<=63) { arr[0]=7; arr[1]=2.3; }
	          else if(a<=70) { arr[0]=8; arr[1]=2.7; }
	            else if(a<=80) { arr[0]=9; arr[1]=3; }
	               else if(a<=90) { arr[0]=10; arr[1]=3.3; }
	                 else if(a<=120) { arr[0]=12; arr[1]=4.2; }
	                   else if(a<=150) { arr[0]=14; arr[1]=4.6; }
	                     else if(a<=180) { arr[0]=16; arr[1]=5.3; }
	                       else if(a<=200) { arr[0]=18; arr[1]=6; }
	                         else if(a<=220) { arr[0]=21; arr[1]=7; }
	                           else if(a>220) { arr[0]=24; arr[1]=8; }

	return arr;
}

function corner(obj,plotn) {
	let height1 = obj.height.value;
	let rad=radius_zao(height1);
	let height = parseFloat(obj.height.value/1000);
	let tolst = parseFloat(obj.tolst.value/1000);
	let lenght = parseFloat(obj.lenght.value);
	let weight = parseFloat(obj.weight.value);
	let R = parseFloat(rad[0]/1000);
	let r = parseFloat(rad[1]/1000);
	if(!lenght) {
		lenght = weight/((((height+height-tolst)*tolst)+(1-Math.PI/4)*(R*R-2*r*r))*plotn);
		$('.lenght input').val(lenght.toFixed(2));
	} else {
		weight = (((height+height-tolst)*tolst)+(1-Math.PI/4)*(R*R-2*r*r))*plotn*lenght;
		$('.weight input').val(weight.toFixed(2));
	}
}

function sheet(obj,plotn) {
	let width = parseFloat(obj.width.value/1000);
	let tolst = parseFloat(obj.tolst.value/1000);
	let lenght = parseFloat(obj.lenght.value/1000);
	let weight = parseFloat(obj.weight.value);

	if(!lenght) {
		lenght = weight/(width*tolst*plotn);
		let square = width*lenght;
		$('.square').empty();
	    $('.square').append('<span>'+square.toFixed(2)+' м<sup>2</sup></span>');
		$('.lenght input').val(lenght.toFixed(2));
		
	} else {
		weight = width*lenght*tolst*plotn;
		let square = width*lenght;
		$('.weight input').val(weight.toFixed(2));
		$('.square').empty();
		$('.square').append('<span>'+square.toFixed(2)+' м<sup>2</sup></span>');
	}
}

function pipe(obj,plotn) {
	let diametr = parseFloat(obj.diametr.value/1000);
	let tolst = parseFloat(obj.tolst.value/1000);
	let lenght = parseFloat(obj.lenght.value);
	let weight = parseFloat(obj.weight.value);
	if(!lenght) {
		lenght = weight/(((Math.PI*Math.pow((diametr/2),2))-(Math.PI*Math.pow((diametr/2-tolst),2)))*plotn);
		$('.lenght input').val(lenght.toFixed(2));
	} else {
		weight = ((Math.PI*Math.pow((diametr/2),2))-(Math.PI*Math.pow((diametr/2-tolst),2)))*lenght*plotn;
		$('.weight input').val(weight.toFixed(2));
	}
}

function circle(obj,plotn) {
	let diametr = parseFloat(obj.diameter.value/1000);
	let lenght = parseFloat(obj.lenght.value);
	let weight = parseFloat(obj.weight.value);
	if(!lenght) {
		lenght = weight/(((Math.PI*diametr*diametr)/4)*plotn);
		$('.lenght input').val(lenght.toFixed(2));
	} else {
		weight = ((Math.PI*diametr*diametr)/4)*lenght*plotn;
		$('.weight input').val(weight.toFixed(2));
	}
}

function pipe_prof(obj,plotn) {
	let height = parseFloat(obj.height.value/1000);
	let width = parseFloat(obj.width.value/1000);
	let tolst = parseFloat(obj.tolst.value/1000);
	let lenght = parseFloat(obj.lenght.value);
	let weight = parseFloat(obj.weight.value);
	if(!lenght) {
		lenght = weight/(2*(width+height-2.86*tolst)*tolst*plotn);
		$('.lenght input').val(lenght.toFixed(2));
	} else {
		weight = 2*(width+height-2.86*tolst)*tolst*lenght*plotn;
		$('.weight input').val(weight.toFixed(2));
	}
}

function square(obj,plotn) {
	let width = parseFloat(obj.width.value/1000);
	let lenght = parseFloat(obj.lenght.value);
	let weight = parseFloat(obj.weight.value);
	if(!lenght) {
		lenght = weight/(width*width*plotn);
		$('.lenght input').val(lenght.toFixed(2));
	} else {
		weight = width*width*lenght*plotn;
		$('.weight input').val(weight.toFixed(2));
	}
}

function channel(obj,plotn) {
	var channelType = parseFloat(obj.channelType.value);
	let lenght = parseFloat(obj.lenght.value);
	let weight = parseFloat(obj.weight.value);
	if(!lenght) {
		lenght = weight/channelType;
		$('.lenght input').val(lenght.toFixed(2));
	} else {
		weight = channelType*lenght;
		$('.weight input').val(weight.toFixed(2));
	}
}

function strip(obj,plotn) {
	let width = parseFloat(obj.width.value/1000);
	let tolst = parseFloat(obj.tolst.value/1000);
	let lenght = parseFloat(obj.lenght.value);
	let weight = parseFloat(obj.weight.value);
	console.log(width,tolst,lenght, weight);
	if(!lenght) {
		lenght = weight/(width*tolst*plotn);
		let square = lenght*width;
		$('.lenght input').val(lenght.toFixed(2));
		$('.square').empty();
	    $('.square').append('<span>'+square.toFixed(2)+' м<sup>2</sup></span>');
	} else {
		weight = width*tolst*lenght*plotn;
		let square = lenght*width;
		$('.square').empty();
	    $('.square').append('<span>'+square.toFixed(2)+' м<sup>2</sup></span>');
		$('.weight input').val(weight.toFixed(2));
	}
}

function balk(obj,plotn) {
	var balkNumber = parseFloat(obj.balkNumber.value);
	let lenght = parseFloat(obj.lenght.value);
	let weight = parseFloat(obj.weight.value);
	if(!lenght) {
		lenght = weight/balkNumber;
		$('.lenght input').val(lenght.toFixed(2));
	} else {
		weight = lenght*balkNumber;
		$('.weight input').val(weight.toFixed(2));
	}
}

function hexahedron(obj,plotn) {
	let radius = parseFloat(obj.diametr.value/1000)/2;
	let lenght = parseFloat(obj.lenght.value);
	let weight = parseFloat(obj.weight.value);
	if(!lenght) {
		lenght = weight/(((2*radius*radius*Math.sqrt(3)))*plotn);
		$('.lenght input').val(lenght.toFixed(2));
	} else {
		weight = ((2*radius*radius*Math.sqrt(3)))*lenght*plotn;
		$('.weight input').val(weight.toFixed(2));
	}
}